package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

type Worker struct {
	IDKaryawan int64
	Nama       string
}

type Staff struct {
	Worker
	Jabatan string
}

func printStaff(s Staff, timeInSec int) {

	time.Sleep(time.Duration(timeInSec) * 1e9)
	fmt.Println(s.IDKaryawan, s.Nama, s.Jabatan)
}

func main1() {
	var staffList []Staff
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Buat Staff")
		fmt.Println("2. Tampilkan Laporan Staff")
		fmt.Println("3. Exit")
		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		if userInput == 3 {
			fmt.Printf("Program Exited")
			break
		}

		switch userInput {
		case 1:
			fmt.Print("Masukan Id: ")
			scanner.Scan()
			id, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Print("Masukan nama: ")
			scanner.Scan()
			nama := scanner.Text()
			fmt.Print("Masukan jabatan: ")
			scanner.Scan()
			jabatan := scanner.Text()

			newWorker := Worker{IDKaryawan: id, Nama: nama}
			newStaff := Staff{
				Worker:  newWorker,
				Jabatan: jabatan,
			}
			staffList = append(staffList, newStaff)

		case 2:
			fmt.Println("id nama jabatan")
			for _, staff := range staffList {
				go printStaff(staff, rand.Intn(3))
			}
			time.Sleep(5 * 1e9)
		}

	}

}
