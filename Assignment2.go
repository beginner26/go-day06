package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type Mahasiswa struct {
	Id    int64
	Nama  string
	Nilai []int64
}

func tampilLayar(mahasiswaList []Mahasiswa) {
	fmt.Println("ID Nama B.Ingrris Fisika Algoritma")
	for _, mhs := range mahasiswaList {
		fmt.Printf("%d %s %d\t %d\t %d \n", mhs.Id, mhs.Nama, mhs.Nilai[0], mhs.Nilai[1], mhs.Nilai[2])
	}
}

func tulisKeFile(mahasiswaList []Mahasiswa, namaFile string) {
	file, err := os.Create(namaFile + ".csv")
	defer file.Close()
	if err != nil {
		log.Fatalln("failed to open file", err)
	}
	w := csv.NewWriter(file)
	defer w.Flush()

	var data [][]string
	data = append(data, []string{"id", "nama", "b.inggris", "fisika", "algoritma"})
	for _, record := range mahasiswaList {
		row := []string{strconv.Itoa(int(record.Id)), record.Nama, strconv.Itoa(int(record.Nilai[0])), strconv.Itoa(int(record.Nilai[1])), strconv.Itoa(int(record.Nilai[2]))}
		data = append(data, row)
	}
	w.WriteAll(data)
}

func main() {
	var mahasiswaList []Mahasiswa
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Create & Input Data Mahasiswa")
		fmt.Println("2. Tampilkan Laporan Nilai Data Mahasiswa")
		fmt.Println("3. Tampilkan di Layar ada 2 (go rutine)")
		fmt.Println("4. Exit")
		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}

		switch userInput {
		case 1:
			fmt.Print("Masukan Id: ")
			scanner.Scan()
			id, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Print("Masukan nama: ")
			scanner.Scan()
			nama := scanner.Text()
			fmt.Print("Masukan Nilai B. Inggris: ")
			scanner.Scan()
			inggris, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Print("Masukan Nilai Fisika: ")
			scanner.Scan()
			fisika, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Print("Masukan Nilai Algoritma: ")
			scanner.Scan()
			algoritma, _ := strconv.ParseInt(scanner.Text(), 10, 64)

			nilai := []int64{inggris, fisika, algoritma}

			newMahasiswa := Mahasiswa{Id: id, Nama: nama, Nilai: nilai}
			mahasiswaList = append(mahasiswaList, newMahasiswa)

		case 2:

			sort.Slice(mahasiswaList, func(i, j int) bool {
				return mahasiswaList[i].Id < mahasiswaList[j].Id
			})

			tampilLayar(mahasiswaList)

		case 3:
			fmt.Print("Masukan nama file yang akan di generate: ")
			scanner.Scan()
			nama := scanner.Text()
			go tampilLayar(mahasiswaList)
			go tulisKeFile(mahasiswaList, nama)

		}

	}
}
