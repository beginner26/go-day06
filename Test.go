package main

import (
	"fmt"
	"strconv"
	"time"
)

func makeCakeAndSend(cs chan string, flavor string, count int) {
	for i := 1; i < count; i++ {
		cakeName := flavor + " Cake " + strconv.Itoa(i)
		cs <- cakeName
	}
	close(cs)
}

func recieveCakeAndPack(strbry_cs chan string, choco_cs chan string) {
	strbry_closed, choco_closed := false, false

	for {
		if strbry_closed && choco_closed {
			return
		}
		fmt.Println("Waiting for a new cake ...")

		select {
		case cakeName, strbry_ok := <-strbry_cs:
			if !strbry_ok {
				strbry_closed = true
				fmt.Println("... Strawberry channel closed!")

			} else {
				fmt.Println("Received from Strawberry channel. Now Pcking", cakeName)
			}

		case cakeName, choco_ok := <-choco_cs:
			if !choco_ok {
				choco_closed = true
				fmt.Println("... Chocolate channel closed!")
			} else {
				fmt.Println("Received from chocolate channel. now packing", cakeName)
			}
		}
	}
}

func maintest() {
	strbry_cs := make(chan string)
	choco_cs := make(chan string)

	go makeCakeAndSend(choco_cs, "Chocolate", 3)
	go makeCakeAndSend(strbry_cs, "Strawberry", 3)

	go recieveCakeAndPack(strbry_cs, choco_cs)

	time.Sleep(2 * 1e9)
}
